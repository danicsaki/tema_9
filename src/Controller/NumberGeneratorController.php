<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class NumberGeneratorController extends AbstractController
{
    /**
     * @Route("/", name="")
     */
    public function index()
    {

        return $this->render('number_generator/index.html.twig', [
            'controller_name' => 'NumberGeneratorController',
        ]);
    }

    
    function generateNumber() {
        $number = rand(0,100);
        echo $number;
    }
}
